require 'rails_helper'

describe Account do

  before(:each) do
  #creating a simple hash for simplicity purpose for now instead of using factory gitl.
  @params = {email: 'test@example.com', password: 'password', password_confirmation: 'password'}
  # @account = FactoryGirl.build(:account)
  end

  context "checking validations" do

    it "should create a new instance of a account" do
      Account.new(@params).should be_valid
    end

  end

  context "persist all the emails and keeping lastly updated email as defaule one" do 

    it "should always keep a default email" do 
      before_count = Email.count
      account = Account.create(@params)
      expect(Email.count).not_to eq(before_count)
      expect (account.email).should eq("test@example.com")
      expect (account.default_email.email).should eq("test@example.com")
    end

    it "should persist multiple emails" do 
      account = Account.create(@params)
      account.send(:save_default_email)
      account.update(email: "another@email.com")
      account.reload
      account.send(:save_default_email)
      expect (account.email).should eq("another@email.com")
      expect (account.emails.size).should eq(2)
      expect (account.emails.pluck(:email)).should eq(["test@example.com","another@email.com"])
    end

  end 


end
