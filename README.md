# backend.challenge

This repository holds the backend development challenge for applicants of Babbel.

## The application

The application in this repository is simplistic. It consists of one model `Account`. The `Account` model is also baked by [devise](https://github.com/plataformatec/devise).

## The task

The customer service department wants to keep track of all the email addresses a user has had in its history as a Babbel customer. The previous email addresses should be persisted.

## Solution

Added another model which will store all the emails of the user and user model will have a default email address id. Email which is updated lastly will become default email.

## Practical

Let's see this into action by opening up Rails console by firing `rails c`

```
> params = {email: 'test@example.com', password: 'password', password_confirmation: 'password'}
> account = Account.create(params)

```

we will have our account created with given params.
```
> account.email
=> "test@example.com"

```

Now, let's add one more email id for the account we created.

```
> account.update(email: "another@email.com")

```
Now, let's ensure we are persiting new and earlier both emails and updating new email as default.and let's check we are storing all the emails.
```
> account.emails
 => #<ActiveRecord::Associations::CollectionProxy [#<Email id: 1, email: "test@example.com", account_id: 2, created_at: "2016-08-07 11:25:37", updated_at: "2016-08-07 11:27:10">, #<Email id: 2, email: "another@email.com", account_id: 2, created_at: "2016-08-07 11:30:59", updated_at: "2016-08-07 11:30:59">]> 
 > account.default_email
  => #<Email id: 2, email: "another@email.com", account_id: 2, created_at: "2016-08-07 11:30:59", updated_at: "2016-08-07 11:30:59">

```

##Tests

Run specs by simply running `rspec`

##Note
Kept the application console controlled, not added views for visualization as it was clear that you wanted to check the approach taken to solve the problem. 