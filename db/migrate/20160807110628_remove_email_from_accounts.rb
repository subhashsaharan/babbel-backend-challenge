class RemoveEmailFromAccounts < ActiveRecord::Migration
  def change
  	remove_column :accounts, :email
    add_column :accounts, :default_email_id, :integer
  end
end
