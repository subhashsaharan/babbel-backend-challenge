class Account < ActiveRecord::Base
  
  devise :confirmable, :database_authenticatable, :lockable,
         :recoverable, :registerable, :rememberable, :validatable

  has_many :emails, dependent: :destroy
  belongs_to :default_email, class_name: "Email"

  validates :default_email, presence: true
  
  after_commit :save_default_email
 
  # adding accessor to tell devise to use our default email for email 
  def email
    default_email.email rescue nil
  end

  def email= email
    self.default_email = Email
      .where(email: email)
      .first_or_initialize
  end

  def email_changed?
  	false
  end


  private

  def save_default_email
    default_email.account = self
    default_email.save!
  end
         
end
